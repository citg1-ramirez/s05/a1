<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import= "java.time.*, java.time.format.DateTimeFormatter" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Activity</title>
</head>
<body>

	<%
		LocalDateTime dateTime = LocalDateTime.now();
		DateTimeFormatter dtformat= DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	%>
	

  	
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila: <%= dateTime.format(dtformat) %> </li>
		<li> Japan: <%= dateTime.plusHours(1).format(dtformat) %> </li>
		<li> Germany: <%= dateTime.minusHours(7).format(dtformat) %></li>
	</ul>
	
	<%!
		private int initVar=3;
		private int serviceVar=3;
		private int destroyVar=3;
		
	  	public void jspInit(){
	    	initVar--;
	    	System.out.println("jspInit(): init"+initVar);
	  		}
	  	public void jspDestroy(){
	    	destroyVar--;
	    	destroyVar = destroyVar + initVar;
	    	System.out.println("jspDestroy(): destroy"+destroyVar);
	  	} 
	%>
	
	<%
		serviceVar--;
	  	System.out.println("_jspService(): service"+serviceVar);
	  	String content1="content1 : "+initVar;
	  	String content2="content2 : "+serviceVar;
	  	String content3="content3 : "+destroyVar;
  	%>
  	
	
	<h1>JSP</h1>
	<p> <%= content1 %> </p>
	<p> <%= content2 %> </p>
	<p> <%= content3 %> </p> 

</body>
</html>
